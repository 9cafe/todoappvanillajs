# Todo App

This is a simple todo app made with Vanilla JS with help from https://watchandcode.com

## 1. Setup

1. [Clone the project locally](https://bitbucket.org/9cafe/betmanager/src/master/)
2. From the root of the project, open up a terminal and either run `yarn install` (if you have
**yarn** installed) or `npm install` to add the required external project dependencies.

### 2. Running

From the root of the project, open up a terminal and run `npm start` to run the code with a local server.